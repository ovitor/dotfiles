# Get OS software updates
alias update='sudo pacman -Suy; trizen -Syyu'

# Simple tree
alias tree="tree -C"
alias t1="tree -L 1"
alias pbpaste="xclip -selection clipboard -o"
alias pbcopy="xclip -selection c"

alias gcds="echo 'conectando a servidor: 200.17.33.66' && xfreerdp /u:'adproducao.ifce.edu.br\Administrator' /v:200.17.33.66"
alias academico="echo 'conectando a servidor: 200.17.33.6' && xfreerdp /u:'qacademicoweb' /v:200.17.33.6"

# simple dev
alias code-python="tmux new-session -s python \; send-keys 'vim .' C-m \; split-window -v -p 25 \; send-keys 'pipenv shell' C-m \; split-window -h -p 25 \; send-keys 'pipenv shell' C-m \; select-pane -t 0"
alias code-laravel="tmux new-session -s laravel \; send-keys 'vim .' C-m \; split-window -v -p 25 \; send-keys 'php artisan serve --host 0.0.0.0' C-m \; split-window -h -p 25 \; send-keys 'npm run watch' C-m \; select-pane -t 0"
alias glog="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
