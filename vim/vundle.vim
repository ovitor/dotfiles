set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'

" code
Plugin 'johnsyweb/vim-makeshift'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'posva/vim-vue'
Plugin 'lervag/vimtex'
Plugin 'hashivim/vim-terraform'

" completion
"Plugin 'neoclide/coc.nvim'
Plugin 'honza/vim-snippets'

" interface
Plugin 'dracula/vim'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'itchyny/lightline.vim'
Plugin 'ryanoasis/vim-devicons'

" others
Plugin 'amiorin/vim-project'
Plugin 'junegunn/fzf'

call vundle#end()
filetype plugin indent on
