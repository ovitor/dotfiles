# Dotfiles

## Overview

This repo is my dotfiles.

## Requirements

1. normal user with sudo powers

## Using this repo

You don't. [Dotfiles Are Not Meant to Be Forked](http://www.anishathalye.com/2014/08/03/managing-your-dotfiles/).

But, if you do:

    $ git clone --recursive git@github.com:vitorcarvalhoml/arch-dotfiles.git ~/.dotfiles
    $ cd .dotfiles
    $ ./install

Remeber to create a tokens file into your home directory. The content is private, so I'm not put it here.

